let simpan = [];
let newData;


let getData0 = () => {
  $(".product").each((x, item) => {
    $(item).click((e) => {
      const pilih = $(e.target);
      if (pilih.hasClass("basket")) {
        show(pilih);
        first(pilih, x);
      } else if (pilih.hasClass("add")) {
        simpan[x].jum += 1;
        $(pilih).prev().html(simpan[x].jum);
      } else if (pilih.hasClass("min")) {
        simpan[x].jum -= 1;
        $(pilih).next().html(simpan[x].jum);
        if (simpan[x].jum == 0) {
          $(pilih).parent().css("display", "none");
          const close = $(pilih).parent().prev();
          $(close).animate({ width: "toggle" });
          $(close).css("display", "flex");
        }
      }
      newData = data(simpan);
      sessionStorage.setItem("item", JSON.stringify(newData));
      let hastot = 0;
      for (let i = 0; i < newData.length; i++) {
        hastot += newData[i].jum;
      }
      notif(hastot);
    });
  });

  bagClick()
  closeBag()

  
};

let bagClick = () => {
  $(".notifbag .bag").click(() => {
    $(".pay").css("display", "flex");
    const get = JSON.parse(sessionStorage.getItem("item"));
    let results = get.filter((element) => {
      if (Object.keys(element).length !== 0) {
        return true;
      }
      return false;
    });

    let size = results.length;

    if (size == 0) {
      $(".pay").append(
        `<div style="align-self:center;" class="empty">
        <h4>Upss Bag is Empty</h4>
      </div>
      `
      );
    } else {
      for (let i = 0; i < size; i++) {
        $(".pay").append(
          `<div class="list">
              <div class="menu">
                  <img src="${results[i].img}" class="list-img">
                  <div class="desk">
                    <h4>${results[i].nama}</h4>
                    <span>Rp ${results[i].harga}</span>
                  </div>
              </div>
              <span class="jum">${results[i].jum}</span>
          </div>
          `
        );
      }
    }
  });
};

const closeBag=()=>{
  $("h2.close").click(()=>{
    $(".pay").css("display","none")
    $(".list").remove();
    $("#payment").remove();
    $(".empty").remove();
  })
}

let data = (simpan) => {
  return simpan.filter((data) => data.jum !== 0);
};

const notif = (hastot) => {
  let notif = $(".notifbag div");
  if (hastot > 0) {
    $(notif).html(hastot);
    $(notif).css("display", "flex");
  } else {
    $(notif).css("display", "none");
  }
};

const show = (e) => {
  $(e).parent().css("display", "none");
  const add = $(e).parent().next();
  $(add).animate({ width: "toggle" });
  $(add).css("display", "flex");
};
const first = (pilih,x)=>{

  $(pilih).parent().css("display","none")
  $(pilih).parent().next().css("display","flex")
  let act1 = (pilih)=>{
    const harga =$(pilih).prev().children().text();
    const img = $(pilih).parent().siblings(".img-product").attr("src");
    const nama =$(pilih).parent().prev().text();
    let jum=0;

    return {harga,img,jum,nama}
  }
  let data = act1(pilih)
  let jum = data.jum+1;
  simpan[x]=data
  simpan[x].jum=jum;
  $(pilih).parent().next().children("h3").html(simpan[x].jum)

  return simpan[x]
}

export { getData0};
